<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMobyUserMobyContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moby_user_moby_contract', function (Blueprint $table) {
	        $table->dropIndex( 'moby_user_moby_contract_moby_table_pk_id_unique' );
            $table->string('moby_table_pk_id', 256 )->nullable()->index()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     //   Schema::dropIfExists('moby_user_moby_contract');
    }
}
