<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobyUserMobyContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moby_user_moby_contract', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('moby_table_pk_id')->unsigned()->unique();

            $table->dateTimeTz( 'valid_from' )->nullable();
            $table->dateTimeTz( 'valid_to' )->nullable();

            $table->integer('user_id')->unsigned()->nullable(false);
            $table->integer('contract_id')->unsigned()->nullable(false);

            $table->foreign('user_id')->references('id')->on('moby_users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('contract_id')->references('id')->on('moby_contracts')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moby_user_moby_contract');
    }
}
