<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobyUserVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moby_user_visit', function (Blueprint $table) {
            $table->increments('id');

	        $table->dateTimeTz('visit');
            $table->integer('user_id')->unsigned()->nullable(false);

            $table->foreign('user_id')->references('id')->on('moby_users')->onDelete('cascade')->onUpdate('cascade');

            $table->unique(['user_id', 'visit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moby_user_visit');
    }
}
