<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moby_contracts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moby_item_id')->unique();
            $table->integer('moby_item_code');
            $table->string('moby_item_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moby_contracts');
    }
}
