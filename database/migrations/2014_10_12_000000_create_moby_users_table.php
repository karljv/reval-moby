<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moby_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moby_id')->unique();
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->dateTime('birthday')->nullable();
            $table->dateTimeTz('last_visit')->nullable();

            $table->dateTimeTz('newsletter_subscription_datetime')->nullable();
	        $table->dateTimeTz('one_time_ticket_purchase_datetime' )->nullable();

            $table->integer('moby_support_trainer_id')->unsigned()->nullable(true);

            $table->foreign('moby_support_trainer_id')->references('id')->on('moby_support_trainers')->onDelete('restrict')->onUpdate('cascade');

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moby_users');
    }
}
