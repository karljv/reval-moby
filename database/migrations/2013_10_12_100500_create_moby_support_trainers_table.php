<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobySupportTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moby_support_trainers', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->integer('moby_id')->unique();

            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->boolean('is_class' )->default(false);
            $table->boolean('is_personal' )->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moby_support_trainers');
    }
}
