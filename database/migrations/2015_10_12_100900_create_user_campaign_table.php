<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_campaign', function (Blueprint $table) {
            $table->increments('id');

	        $table->string( "campaign_identifier" );

	        $table->integer('user_id')->unsigned()->nullable();
	        $table->integer('external_user_id')->unsigned()->nullable();

	        $table->boolean( "is_closed" )->default(false);
	        $table->dateTimeTz( 'start_datetime' );
	        $table->dateTimeTz( 'end_datetime' )->nullable();

	        $table->foreign('user_id')->references('id')->on('moby_users')->onDelete('restrict')->onUpdate('cascade');
	        $table->foreign('external_user_id')->references('id')->on('external_users')->onDelete('restrict')->onUpdate('cascade');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_campaign');
    }
}
