<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 21/02/2018
 * Time: 14:48
 */

namespace App\Engines;

use App\Models\Cron;
use App\Models\ExternalUser;
use App\Models\UserCampaign;
use App\Services\AMNewsletterSubscription;
use App\Services\AMOfferYearlySubscription;
use App\Services\AMOneTimeTicket;
use App\Services\AMPersonalTrainingOffer;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class BI {
	public function __construct() { }

	public function runDailyCron() {
		/*
		$AMOneTimeTicket = new AMOneTimeTicket();
		$AMOneTimeTicket->setEmail( "karl@brainbase.com" );
		$AMOneTimeTicket->addUserToMailQueue();
		*/

		$this->syncMobyUsersAndExternalUsers();

		$AMNewsletterSubscription = new AMNewsletterSubscription();
		$AMNewsletterSubscription->cron();
		
		$AMOneTimeTicket = new AMOneTimeTicket();
		$AMOneTimeTicket->cron();

		/*$AMOfferYearlySubscription = new AMOfferYearlySubscription();
		$AMOfferYearlySubscription->cron();*/

		//Cant do, because I dont know who has used what personal trainings.
		/*$personalTrainingOffer = new AMPersonalTrainingOffer();
		$personalTrainingOffer->cron();*/


		if ( ! $cron = Cron::find( 1 ) ) {
			$cron = new Cron();
		}

		$cron->last_run = date( 'Y-m-d H:i:s' );

		$cron->saveOrFail();

		echo "<br>FIN";
	}

	public function syncMobyUsersAndExternalUsers() {
		DB::beginTransaction();

		try {
			$deletableUsers = ExternalUser::whereHas( 'user' )->get();

			foreach( $deletableUsers as $extUser ) {
				$user = $extUser->user;

				$user->one_time_ticket_purchase_datetime = $extUser->one_time_ticket_purchase_datetime;
				$user->newsletter_subscription_datetime = $extUser->newsletter_subscription_datetime;

				$user->saveOrFail();

				foreach ( UserCampaign::where( 'external_user_id', $extUser->id )->get() as $campaign ) {
					$campaign->external_user_id = null;
					$campaign->user_id = $user->id;

					$campaign->saveOrFail();
				}

				if ( $extUser->delete() === false ) throw new \Exception( "Failed to delete EXT USER" );
			}
			DB::commit();
		} catch ( \Exception $e ) {
			DB::rollBack();
			error_log( "ERR > #1" );
			die;
		}
	}
}