<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobyUserGroupTrainingVisit extends Authenticatable
{
    protected $table = "moby_user_group_training_visit";
    public $timestamps = false;
}
