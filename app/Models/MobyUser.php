<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobyUser extends Authenticatable
{
    protected $table = "moby_users";
    public $timestamps = true;

    public function mobySupportTrainer() {
        return $this->hasMany( '\App\Models\MobySupportTrainer', 'id', 'support_trainer_id' );
    }

    public function contracts() {
        return $this->belongsToMany( '\App\Models\MobyContract', 'moby_user_moby_contract', 'user_id', 'contract_id', 'moby_contracts' )->withPivot(['valid_from', 'valid_to']);
    }

	public function visits() {
		return $this->hasMany( '\App\Models\MobyUserVisit', 'user_id', 'id' );
	}
}
