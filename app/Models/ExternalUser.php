<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ExternalUser extends Authenticatable
{
    protected $table = "external_users";
    public $timestamps = true;

	public function user() {
		return $this->hasOne( '\App\Models\MobyUser', 'email', 'email' );
	}
}
