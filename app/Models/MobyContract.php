<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobyContract extends Authenticatable
{
    protected $table = "moby_contracts";
    public $timestamps = false;
}
