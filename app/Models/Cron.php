<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cron extends Authenticatable
{
    protected $table = "cron";
    public $timestamps = true;
}
