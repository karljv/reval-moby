<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobyUserVisit extends Authenticatable
{
    protected $table = "moby_user_visit";
    public $timestamps = false;
}
