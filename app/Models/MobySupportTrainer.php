<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobySupportTrainer extends Authenticatable
{
    protected $table = "moby_support_trainers";
    public $timestamps = false;
}
