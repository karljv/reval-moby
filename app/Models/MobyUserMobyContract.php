<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobyUserMobyContract extends Authenticatable
{
    protected $table = "moby_user_moby_contract";
    public $timestamps = false;

	public function contract() {
		return $this->hasOne( '\App\Models\MobyContract', 'id', 'contract_id' );
	}
}
