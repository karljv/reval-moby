<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserCampaign extends Authenticatable
{
    protected $table = "user_campaign";
    public $timestamps = true;

    public function user() {
    	return $this->hasOne( '\App\Models\MobyUser', 'id', 'user_id' );
    }

	public function externalUser() {
		return $this->hasOne( '\App\Models\ExternalUser', 'id', 'external_user_id' );
	}

	public function sentEmails() {
		return $this->hasMany( '\App\Models\SentEmail', 'user_campaign_id', 'id' );
	}

	public function sentEmailsOrderedDESC() {
		return $this->hasMany( '\App\Models\SentEmail', 'user_campaign_id', 'id' )->orderBy( 'id', 'DESC' );
	}
}
