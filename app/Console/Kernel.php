<?php

namespace App\Console;

use App\Engines\BI;
use App\Services\MobyClientsService;
use App\Services\MobyItemListService;
use App\Services\MobySupportTrainerService;
use App\Services\MobyUserService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
	    Log::info( "[schedule]" );
	    $schedule->call(function () {
		    Log::info( "Starting to process TIMELINE." );

		    $supportTrainerService = new MobySupportTrainerService();
		    $supportTrainerService->processAndSaveItems();

		    $itemListService = new MobyItemListService();
		    $itemListService->processAndSaveItems();

		    $userService = new MobyUserService();
		    $userService->processAndSaveItems();

		    $clientsService = new MobyClientsService();
		    $clientsService->processAndSaveItems();

		    Log::info( "Done processing TIMELINE." );
	    })->twiceDaily(1, 13);

	    $schedule->call(function () {
		    Log::info( "Starting DAILY CRON." );

		    $bi = new BI();
		    $bi->runDailyCron();

		    Log::info( "Done DAILY CRON." );
	    })->twiceDaily(2, 14);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
