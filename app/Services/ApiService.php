<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

class ApiService {
    const EP_USER_LIST = "http://moby.ee/booking/revalsport_e761dd84b6429ee6fe9496e21fb86feb/xml/bron_db.php?act=userlist";
    const EP_ITEM_LIST = "http://moby.ee/booking/revalsport_e761dd84b6429ee6fe9496e21fb86feb/xml/bron_db.php?act=itemlist";
    const EP_CLIENTS = "http://moby.ee/booking/revalsport_e761dd84b6429ee6fe9496e21fb86feb/xml/bron_db.php?act=clients";
    const EP_TRAINER_LIST = "http://moby.ee/booking/revalsport_e761dd84b6429ee6fe9496e21fb86feb/xml/bron_db.php?act=trainer_list";

    public function __construct() { }

    public function initCurl() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "accept-encoding: gzip, deflate",
                "accept-language: en-GB,en;q=0.9,en-US;q=0.8,et;q=0.7",
                "cache-control: max-age=0",
                "connection: keep-alive",
                "host: moby.ee",
                "upgrade-insecure-requests: 1",
                "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
            ),
        ));

        return $curl;
    }

    public function makeRequest( $curl ) {
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }

    public function makeRequestAndReturnXML( $curl ) {
	    set_time_limit(600);
	    curl_setopt($curl, CURLOPT_TIMEOUT,600); // 600 seconds

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        	var_dump( $err ); die;
        	return false;
        } else {
	        return simplexml_load_string( $response );
        }
    }

    public function getUserList() {
        $curl = $this->initCurl();
        curl_setopt( $curl, CURLOPT_URL, ApiService::EP_USER_LIST );
        return $this->makeRequestAndReturnXML( $curl );
    }

    public function getItemList() {
        $curl = $this->initCurl();
        curl_setopt( $curl, CURLOPT_URL, ApiService::EP_ITEM_LIST );
        return $this->makeRequestAndReturnXML( $curl );
    }

    public function getClients() {
        $curl = $this->initCurl();
        curl_setopt( $curl, CURLOPT_URL, ApiService::EP_CLIENTS );
        return $this->makeRequestAndReturnXML( $curl );
    }

    public function getTrainerList() {
        $curl = $this->initCurl();
        curl_setopt( $curl, CURLOPT_URL, ApiService::EP_TRAINER_LIST );
        return $this->makeRequestAndReturnXML( $curl );
    }

}