<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\MobyContract;
use App\Models\MobySupportTrainer;
use App\Models\MobyUser;
use App\Models\MobyUserMobyContract;

class MobySupportTrainerService {
    private $apiService;

    public function __construct() {
        $this->apiService = new ApiService();
    }

    public function processAndSaveItems() {
        $xml = $this->apiService->getTrainerList();

        if ( ! $xml ) throw new \Exception( "Endpoint is down." );

        foreach( $xml->trainer as $record ) {
            $mobyId = (int) $record->ID;

            if ( ! $supportTrainer = MobySupportTrainer::where( 'moby_id', $mobyId )->first() ) {
                $supportTrainer = new MobySupportTrainer();
            }

            $supportTrainer->moby_id = $mobyId;
            $supportTrainer->name = (string) $record->Name;
            $supportTrainer->email = (string) $record->Email;
            $supportTrainer->phone = (string) $record->Phone;
            $supportTrainer->is_class = (bool) $record->ClassTrainer;
            $supportTrainer->is_personal = (bool) $record->PersonalTrainer;


            if ( ! $supportTrainer->save() ) throw new \Exception( "Failed to save moby personal trainer." );
        }
    }
}