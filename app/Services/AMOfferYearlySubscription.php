<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Models\SentEmail;
use App\Models\UserCampaign;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Webpatser\Uuid\Uuid;

class AMOfferYearlySubscription extends AMBaseService {
	//num on IMG: 1 - AM: 30 päevased liikmed

	/**
	 *  nr 1 on kuukaardi liikmed, mitte korra kaardi
	 *
	 *  ja esimene meil 5 päeva enne lõppu, teine 3 ja viimane viimasel päeva
	 */

	const ID_CAMP                = "CAMP_YEARLY_SUB";

	const ID_MAIL_FIRST          = 'YEARLY__FIRST';
	const ID_MAIL_SECOND         = 'YEARLY__SECOND';
	const ID_MAIL_THIRD          = 'YEARLY__THIRD';

	const TIME_HOURS_UNTIL_SECOND_EMAIL= 48;   //1464    - 2 months (aka. 61 days)
	const TIME_HOURS_UNTIL_THIRD_EMAIL = 72;   //168

	//const MOBY_ITEM_IDS_THAT_FIT_HERE = [ 577, 1055, 576, 1053, 578, 1504, 652, 585, 584 ];
	const MOBY_ITEM_IDS_THAT_FIT_HERE = [ 576 ];
	const IDS_MOBY_YEARLY_PACKAGES = [ 693, 760 ]; //The ones that the emails suggest to buy!

	public function __construct() {
		$this->init();
	}

	public function init() {
		parent::init();

		$this->setListId( "daa6044606" );
		$this->campaignId = self::ID_CAMP;
	}

	public function cron() {
		Log::debug( "=============== OFFER YEARLY SUBSCRIPTION ===============" );
		$this->addNewUsersToCampaigns( $this->calcUsersToAdd() );

		foreach ( $this->getActiveCampaigns() as $campaign ) {
			if ( $campaign->user ) {
				$this->setEmail( $campaign->user->email );
				$this->parmUser( $campaign->user );
			} else {
				Log::debug( ":AM> [DATA ERROR #A0002] External users shouldn't exist here. You need to be a moby user to get here." );
				continue;
			}

			$lastEmailSent = $campaign->sentEmailsOrderedDESC->first();
			$userCampaign = $this->getUserCampaign();
			switch ( $lastEmailSent->email_identifier ) {
				case self::ID_MAIL_FIRST:
					$this->sendSecondEmail();
					break;

				case self::ID_MAIL_SECOND:
					$this->sendThirdEmail();
					break;

				case self::ID_MAIL_THIRD:
					$this->endUserCampaign( $userCampaign );
					break;
			}
		}
	}

	/**
	 * SELECT user.email, contract.moby_item_name  FROM `reval-moby`.moby_users as user
	JOIN
	moby_user_moby_contract as c
	ON
	c.user_id = user.id
	JOIN
	moby_contracts as contract
	ON
	contract.id = c.contract_id
	WHERE
	contract.moby_item_id IN (577, 1055, 576, 1053, 578, 1504, 652, 585, 584 )
	;
	 *
	 */
	public function calcUsersToAdd() {
		$res = $this->getUsersWhoseContractMobyItemIdInAndNotIn( self::MOBY_ITEM_IDS_THAT_FIT_HERE, self::IDS_MOBY_YEARLY_PACKAGES );
		$ret = [];

		foreach( $res as $user ) {
			if ( $this->mobyUserHasEverHadCampaign( $user->id ) ) {
				continue;
			}

			if ( count( $this->contractsWithIdsWithLessThanXDaysRemaining( self::MOBY_ITEM_IDS_THAT_FIT_HERE, 5, $user->id ) ) === 0 ) {
				continue;
			}

			if ( ! count( $this->mobyUserHasActiveCampaign( $user->id ) ) ) {
				if ( ! $user->email ) {
					Log::debug( ":AM> [DATA ERROR #A0001] User with local ID $user->id, does not have an email. Cannot list him to campaign." );
					continue;
				}

				Log::debug( ":AM> $user->email will be sent |- offer for a yearly subscription. [" . date("d.m.Y") . "]" );

				$ret[] = $user;
			}
		}

		/*if ( env('DEVMODE') && false ) { }*/

		return $ret;
	}

	public function addNewUsersToCampaigns( $_users ) {
		foreach( $_users as $user ) {
			//echo $user->id . " > " . $user->email . "<br>";
			$this->setEmail( $user->email );
			$this->parmUser( $user );
			$this->sendFirstEmail();
		}
	}

	public function canSendFirstEmail() {
		if ( ! $this->parmUser() ) return false;
		if ( $this->userHasCampaign( $this->parmUser()->id ) ) return false;

		return true;
	}

	/**
	 * Send the first email in the sequence.
	 *
	 * @return bool
	 */
	public function sendFirstEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendFirstEmail() ) {
			return false;
		}


		$this->addEmailToList( $this->getEmail() );

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/6350d5b01b/emails/6a239e4acf/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					var_dump( $this->getEmail() );
					var_dump( $resObj );
					//ERROR
					break;

				default:
					var_dump( $this->getEmail() );
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			if ( ! $userCampaign = $this->getUserCampaign() ) {
				$userCampaign = new UserCampaign();

				$userOrExternalUser = $this->getUser();
				switch ( true )
				{
					case $userOrExternalUser instanceof MobyUser:
						$userCampaign->user_id = $userOrExternalUser->id;
						break;

					case $userOrExternalUser instanceof ExternalUser:
						$userCampaign->external_user_id = $userOrExternalUser->id;
						break;
				}

				$userCampaign->start_datetime = date('Y-m-d H:i:s' );
				$userCampaign->campaign_identifier = self::ID_CAMP;

				$userCampaign->saveOrFail();
			}

			$sentEmail->user_campaign_id = $userCampaign->id;
			$sentEmail->email_identifier = self::ID_MAIL_FIRST;

			$sentEmail->saveOrFail();

			Log::debug( ":AM> " . $this->getEmail() . " |- offer for a yearly subscription -| SENT. [" . date("d.m.Y") . "]" );
		}
	}

	public function canSendSecondEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === self::ID_MAIL_FIRST ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, self::TIME_HOURS_UNTIL_SECOND_EMAIL ) ) {
				//If user already has a yearly package, then exclude him and close campaing
				if ( ! count( $this->userHasValidContractInTimeFromIds( new Carbon(), $this->parmUser()->id, self::IDS_MOBY_YEARLY_PACKAGES ) ) > 0 ) {
					return true;
				} else {
					$this->endUserCampaign( $userCampaign );
				}

			}
		}

		return false;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendSecondEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendSecondEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/6350d5b01b/emails/b37efcfabe/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = self::ID_MAIL_SECOND;

			$sentEmail->saveOrFail();
		}
	}

	public function canSendThirdEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === self::ID_MAIL_SECOND ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, self::TIME_HOURS_UNTIL_THIRD_EMAIL ) ) {
				//If user already has a yearly package, then exclude him and close campaing
				if ( ! count( $this->userHasValidContractInTimeFromIds( new Carbon(), $this->parmUser()->id, self::IDS_MOBY_YEARLY_PACKAGES ) ) > 0 ) {
					return true;
				} else {
					$this->endUserCampaign( $userCampaign );
				}

			}
		}

		return false;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendThirdEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendThirdEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/6350d5b01b/emails/9a6e75e007/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = self::ID_MAIL_THIRD;

			$sentEmail->saveOrFail();
		}
	}

}


































