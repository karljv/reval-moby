<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\MobyContract;
use App\Models\MobyUser;
use App\Models\MobyUserGroupTrainingVisit;
use App\Models\MobyUserMobyContract;
use App\Models\MobyUserVisit;

class MobyUserService {
    private $apiService;

    public function __construct() {
        $this->apiService = new ApiService();
    }

    public function processAndSaveItems() {
        $xml = $this->apiService->getUserList();

        if ( ! $xml ) throw new \Exception( "Endpoint is down." );

        foreach( $xml->RECORD as $record ) {
            $mobyUserId = (int) $record->ID;

            if ( ! $mobyUser = MobyUser::where( 'moby_id', $mobyUserId )->first() ) {
                $mobyUser = new MobyUser();
            }

            $mobyUser->moby_id = $mobyUserId;
            $mobyUser->email = (string) $record->EMAIL;
            $mobyUser->first_name = (string) $record->EESNIMI;
            $mobyUser->last_name = (string) $record->PEREKONNANIMI;

            $birthDay = (string) $record->SYNNIPAEV;
            if ( $birthDay !== '0000-00-00' ) {
                $mobyUser->birthday = date( 'Y-m-d', strtotime( $birthDay ) );
            }

            $lastVisit = (string) $record->LASTVISIT;
            if ( $lastVisit !== '0000-00-00' ) {
                $mobyUser->last_visit = date( 'Y-m-d', strtotime( $lastVisit ) );
            }

	        if ( ! $mobyUser->save() ) throw new \Exception( "Failed to save moby user." );

	        //$mobyUserVisit = new MobyUserVisit();
	        if ( $mobyUser->last_visit ) {
		        if ( ! $mobyUserVisit = MobyUserVisit::where( 'moby_user_visit.user_id', $mobyUser->id )->where( 'moby_user_visit.visit', date("Y-m-d H:i:s", strtotime($mobyUser->last_visit) ) )->first() ) {
			        $mobyUserVisit = new MobyUserVisit();

			        $mobyUserVisit->user_id     = $mobyUser->id;
			        $mobyUserVisit->visit       = $mobyUser->last_visit;

			        $mobyUserVisit->save();
		        }
	        }
        }

	    //Last group training visit
	    $xml = $this->apiService->getClients();

	    if ( ! $xml ) throw new \Exception( "Endpoint is down." );

	    foreach( $xml->client as $record ) {
		    $mobyUserId = (int) $record->ID;

		    if ( ! $mobyUser = MobyUser::where( 'moby_id', $mobyUserId )->first() ) {
			    continue;
		    }

		    $lastGroupVisit = (string) $record->ClassesLastVisit;
		    if ( ! $lastGroupVisit ) {
			    continue;
		    }

		    if ( $mobyUserGroupTrainingVisit = MobyUserGroupTrainingVisit::where( 'visit', $lastGroupVisit )->where( 'user_id', $mobyUser->id )->exists() ) continue;
		    $mobyUserGroupTrainingVisit = new MobyUserGroupTrainingVisit();

		    $mobyUserGroupTrainingVisit->user_id = $mobyUser->id;
		    $mobyUserGroupTrainingVisit->visit   = $lastGroupVisit;

		    $mobyUserGroupTrainingVisit->save();

		    /*try {
				//if ( ! $mobyUser->save() ) throw new \Exception( "Failed to save moby user." );
			} catch ( \Exception $e ) {
				echo 1;
				throw new \Exception( "Failed to save moby user." );
			}*/
	    }
    }
}