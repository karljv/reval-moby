<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\MobyContract;
use App\Models\MobySupportTrainer;
use App\Models\MobyUser;
use App\Models\MobyUserMobyContract;
use Webpatser\Uuid\Uuid;

class MobyClientsService {
    private $apiService;

    public function __construct() {
        $this->apiService = new ApiService();
    }

    public function processAndSaveItems() {
        $xml = $this->apiService->getClients();
        if ( ! $xml ) throw new \Exception( "Endpoint is down." );

        foreach( $xml->client as $client ) {
            $mobyUserId = (int) $client->ID;

            if ( ! $mobyUser = MobyUser::where( 'moby_id', $mobyUserId )->first() ) {
                $mobyUser = new MobyUser();
            }

            $mobyUser->moby_id      = $mobyUserId;
            $mobyUser->email        = (string) $client->Email;
            $mobyUser->first_name   = (string) $client->FirstName;
            $mobyUser->last_name    = (string) $client->LastName;

            if ( $mobySupportTrainer = MobySupportTrainer::where( 'moby_id', (int) $client->TrainerID )->first() ) {
            	$mobyUser->moby_support_trainer_id = $mobySupportTrainer->id;
            }

            $birthDay = (string) $client->SYNNIPAEV;
            if ( $birthDay !== '0000-00-00' ) {
                $mobyUser->birthday = date( 'Y-m-d', strtotime( $birthDay ) );
            }

            $lastVisit = (string) $client->LASTVISIT;
            if ( $lastVisit !== '0000-00-00' ) {
                $mobyUser->last_visit = date( 'Y-m-d', strtotime( $lastVisit ) );
            }

            if ( ! $mobyUser->save() ) throw new \Exception( "Failed to save moby user." );

            if ( $mobyUser->id == 5425 ) {
            	echo 1;
            }

            foreach ( $client->contracts as $contract ) {
                if ( property_exists( $contract, "contract" ) ) {
                	$contract = $contract->contract;
                }

	            $contractMobyTablePkId = (int) $contract->ID;
                $contractMobyId = (int) $contract->ItemID;
                $contractMobyCode = (int) $contract->ItemCode;
                $contractName = (string) $contract->ItemName;

	            $validFrom = (string) $contract->ValidFrom;
	            if ( $validFrom !== '0000-00-00' ) {
		            $validFrom = date( 'Y-m-d', strtotime( $validFrom ) );
	            } else {
		            $validFrom = null;
	            }

	            $validTo = (string) $contract->ValidTill;
	            if ( $validTo !== '0000-00-00' ) {
		            $validTo = date( 'Y-m-d', strtotime( $validTo ) );
	            } else {
		            $validTo = null;
	            }

                if ( ! $mobyItem = MobyContract::where( 'moby_item_id', $contractMobyId )->first() ) {
		            $mobyItemListService = new MobyItemListService();
		            if ( ! $mobyItem = $mobyItemListService->createOrUpdate( $contractMobyId, $contractMobyCode, $contractName ) ) {
						throw new \Exception( "Cannot find or create contract. #3848848" );
		            }
				}

				if ( $contractMobyTablePkId ) {
                	if ( ! $mobyUserMobyContract = MobyUserMobyContract::where('moby_table_pk_id', $contractMobyTablePkId )->first() ) {
						$mobyUserMobyContract = new MobyUserMobyContract();

						$mobyUserMobyContract->moby_table_pk_id = $contractMobyTablePkId;
					}
				} else {
					if ( ! $mobyUserMobyContract = MobyUserMobyContract::where('contract_id', $mobyItem->id )
																		->whereDate( 'valid_from', $validFrom )
																		->whereDate( 'valid_to', $validTo )
					                                                    ->where( 'user_id', $mobyUser->id )->first() ) {
						$mobyUserMobyContract = new MobyUserMobyContract();

						$mobyUserMobyContract->moby_table_pk_id = Uuid::generate(4);
					}
				}

	            $mobyUserMobyContract->user_id = $mobyUser->id;
	            $mobyUserMobyContract->contract_id = $mobyItem->id;
	            $mobyUserMobyContract->valid_from = $validFrom;
	            $mobyUserMobyContract->valid_to = $validTo;

	            if ( isset( $contract->Price ) ) {
		            $mobyUserMobyContract->price = (float) $contract->Price;
	            }

	            if ( ! $mobyUserMobyContract->save() ) throw new \Exception( "Failed to save item." );
            }
        }
    }
}