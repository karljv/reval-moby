<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Models\SentEmail;
use App\Models\UserCampaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class AMSlackers extends AMBaseService {
	//num on IMG: 4 - AM: Passiivsed treenijad

	const ID_CAMP                       = "CAMP_SLACKING";

	const ID_MAIL_SUB                   = 'SLACKERS_MAIL_1';

	const MOBY_ITEM_IDS_THAT_FIT_HERE = [ 577, 1055, 576, 1053, 578, 1504, 652, 585, 584, 693, 760 ];

	public function __construct() {
		$this->init();
	}

	public function init() {
		parent::init();

		$this->setListId( "771e11056d" );
		$this->campaignId = self::ID_CAMP;
	}

	public function cron() {
		Log::debug( "=============== SLACKERS ===============" );
		$this->addNewUsersToCampaigns( $this->calcUsersToAdd() );
	}

	public function addNewUsersToCampaigns( $_users ) {
		foreach( $_users as $user ) {
			//echo $user->id . " > " . $user->email . "<br>";
			$this->setEmail( $user->email );
			$this->parmUser( $user );
			$this->sendFirstEmail();
		}
	}

	public function calcUsersToAdd() {
		$res = $this->getUsersWhoseContractMobyItemIdInAndNotIn( self::MOBY_ITEM_IDS_THAT_FIT_HERE, [] );
		$ret = [];

		foreach( $res as $user ) {
			if ( $this->mobyUserHasEverHadCampaign( $user->id ) ) {
				continue;
			}

			$visits = count( $this->getVisitsInTheLast( $user->id, "2 weeks ago" ) );
			if ( $visits !== 0 ) {
				continue;
			}

			if ( ! $user->email ) {
				Log::debug( ":AM> [DATA ERROR #A0001] User with local ID $user->id, does not have an email. Cannot list him to campaign." );
				continue;
			}

			Log::debug( ":AM> $user->email will be sent |- slacker notice. [" . date("d.m.Y") . "]" );
			//var_dump( $user );

			$ret[] = $user;
		}

		/*if ( env('DEVMODE') && false ) { }*/

		return $ret;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendFirstEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		//$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/ae8f2ddcd8/emails/c5542c04c7/queue" );
		$resObj = true;

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "SLACKER SUB::ID_MAIL_SUB> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "SLACKER SUB::ID_MAIL_SUB> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			if ( ! $userCampaign = $this->getUserCampaign() ) {
				$userCampaign = new UserCampaign();

				$userOrExternalUser = $this->getUser();
				switch ( true )
				{
					case $userOrExternalUser instanceof MobyUser:
						$userCampaign->user_id = $userOrExternalUser->id;
						break;

					case $userOrExternalUser instanceof ExternalUser:
						$userCampaign->external_user_id = $userOrExternalUser->id;
						break;
				}

				$userCampaign->start_datetime = date('Y-m-d H:i:s' );
				$userCampaign->campaign_identifier = self::ID_CAMP;

				$userCampaign->saveOrFail();
			}

			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $userCampaign->id;
			$sentEmail->email_identifier = self::ID_MAIL_SUB;

			$sentEmail->saveOrFail();
		}
	}

}