<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Models\MobyUserMobyContract;
use App\Models\MobyUserVisit;
use App\Models\SentEmail;
use App\Models\UserCampaign;
use Carbon\Carbon;

class AMBaseService {
	private   $user;
	private   $mailChimp;
	private   $apiKey = "11c3eaf0ffa202a64b953a399711b46f-us4";
	private   $listId;
	protected $campaignId;
	private   $email;

	public function __construct() {
		$this->init();
	}

	public function init() {
		if ( ! $this->apiKey ) {
			return false;
		}

		$this->mailChimp = new \DrewM\MailChimp\MailChimp( $this->apiKey );
	}

	public function setListId( string $_listId ) {
		$this->listId = $_listId;
	}

	public function getListId() {
		return $this->listId;
	}

	public function getMailChimp() {
		return $this->mailChimp;
	}

	protected function getApiKey() {
		return $this->apiKey;
	}

	public function setEmail( string $_email ) {
		$this->email = $_email;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getListSubscribers() {
		if ( ! $this->listId ) return false;

		$result = $this->mailChimp->get( "lists/" . $this->listId . "/members", [
			'count' => 100000
		]);

		return $result;
	}

	public function addUserToList() {
		if ( ! $this->listId || ! $this->getEmail() ) {
			return false;
		}

		$result = $this->mailChimp->post( "lists/" . $this->listId . "/members", [
			'email_address' => $this->email,
			'status'        => 'subscribed',
		] );

		return $result;
	}

	public function executeCurl( string $_endpoint, string $_email = null ) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/

		$email = ( $_email ) ? $_email : $this->email;

		$ch = curl_init();

		curl_setopt( $ch, CURLOPT_URL, $_endpoint );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, "{\"email_address\": \"" . $email . "\"}" );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_USERPWD, "anystring" . ":" . $this->getApiKey() );

		$headers   = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

		$result = curl_exec( $ch );
		if ( curl_errno( $ch ) ) {
			echo 'Error:' . curl_error( $ch );
		}
		curl_close( $ch );

		return json_decode( $result );
	}

	public function getUserCampaign() {
		if ( ! $user = MobyUser::where( 'email', $this->email )->first() ) {
			$externalUser = ExternalUser::where( 'email', $this->email )->first();
		}

		$userCampaign = new UserCampaign();

		if ( $user ) {
			return $userCampaign->where( 'user_id', $user->id )->where( 'campaign_identifier', $this->campaignId )->first();
		} else {
			return $userCampaign->where( 'external_user_id', $externalUser->id )->where( 'campaign_identifier', $this->campaignId )->first();
		}
	}

	public function getUser() {
		if ( ! $user = MobyUser::where( 'email', $this->getEmail() )->first() ) {
			return ExternalUser::where( 'email', $this->getEmail() )->first();
		} else {
			return $user;
		}
	}

	public function getActiveCampaigns() {
		return UserCampaign::where( 'campaign_identifier', $this->campaignId )
		                   ->where( 'is_closed', false )
		                   ->get();
	}

	public function mobyUserHasActiveCampaign( int $_userId, $_campaignId = null ) {
		$campaignId = ( $_campaignId ) ? $_campaignId : $this->campaignId;
		return UserCampaign::where( 'campaign_identifier', $campaignId )
		                   ->where( 'is_closed', false )
		                   ->where( 'user_id', $_userId )
		                   ->get();
	}

	public function mobyUserHasEverHadCampaign( int $_userId, $_campaignId = null ) {
		$campaignId = ( $_campaignId ) ? $_campaignId : $this->campaignId;
		return UserCampaign::where( 'campaign_identifier', $campaignId )
		                   ->where( 'user_id', $_userId )
		                   ->exists();
	}

	public function endUserCampaign( UserCampaign $_userCampaign ) {
		$_userCampaign->end_datetime = date( 'Y-m-d H:i:s' );
		$_userCampaign->is_closed    = true;

		return $_userCampaign->save();
	}

	public function getLastSentEmailInCampaign( UserCampaign $_userCampaign ) {
		return $_userCampaign->sentEmailsOrderedDESC->first();
	}

	public function isItTimeToSendNextEmail( SentEmail $_lastEmail, int $_timeToWait ) {
		return ( $_timeToWait - ( new Carbon( $_lastEmail->created_at, 'UTC' ) )->diffInHours() < 0 );
	}

	public function userHasValidContractInTimeFromIds( Carbon $_whenValid, int $_userId, Array $_contractIds ) {
		return MobyUserMobyContract::where( "user_id", $_userId )
		                           ->whereDate( 'valid_from', '<', $_whenValid )
		                           ->whereDate( 'valid_to', '>', $_whenValid )
		                           ->whereIn( 'contract_id', $_contractIds )
		                           ->get();
	}

	public function getUsersWhoseContractMobyItemIdIn( Array $_mobyContractItemIds ) {
		return MobyUser::whereHas( 'contracts', function ( $q ) use ( $_mobyContractItemIds ) {
			$q->whereIn( 'moby_item_id', $_mobyContractItemIds );
		} )->get();
	}

	public function getUsersWhoseContractMobyItemIdInAndContractIsValid( Array $_mobyContractItemIds ) {
		return MobyUser::whereHas( 'contracts', function ( $q ) use ( $_mobyContractItemIds ) {
			$q->whereIn( 'moby_item_id', $_mobyContractItemIds );
			$q->where('moby_user_moby_contract.valid_to', '>', Carbon::now() );
		} )->get();
	}

	public function getUsersWhoseContractMobyItemIdInAndContractIsValidAndValidFromIsLessThan( Array $_mobyContractItemIds, Carbon $_validFrom ) {
		return MobyUser::whereHas( 'contracts', function ( $q ) use ( $_mobyContractItemIds, $_validFrom ) {
			$q->whereIn( 'moby_item_id', $_mobyContractItemIds );
			$q->where('moby_user_moby_contract.valid_to', '>', Carbon::now() );
			$q->where('moby_user_moby_contract.valid_from', '<', $_validFrom );
		} )->get();
	}

	public function getUsersWhoseContractMobyItemIdInAndNotIn(
		Array $_mobyContractItemIds,
		Array $_mobyContractItemIdsNotIn
	) {
		return MobyUser::whereHas( 'contracts', function ( $q ) use (
			$_mobyContractItemIds,
			$_mobyContractItemIdsNotIn
		) {
			$q->whereIn( 'moby_item_id', $_mobyContractItemIds );
			$q->whereNotIn( 'moby_item_id', $_mobyContractItemIdsNotIn );
		} )->get();
	}

	public function getUsersWhoseContractMobyItemIdInAndNotInAndPriceEquals(
		Array $_mobyContractItemIds,
		Array $_mobyContractItemIdsNotIn,
		float $_price
	) {
		return MobyUser::whereHas( 'contracts', function ( $q ) use (
			$_mobyContractItemIds,
			$_mobyContractItemIdsNotIn,
			$_price
		) {
			$q->whereIn( 'moby_item_id', $_mobyContractItemIds );
			$q->whereNotIn( 'moby_item_id', $_mobyContractItemIdsNotIn );
			$q->whereRaw( 'moby_user_moby_contract.price = ' . $_price );
		} )->get();
	}

	public function getVisitsInTheLast( int $_userId, string $_inTheLast ) {
		$carbon = new Carbon( $_inTheLast );

		return MobyUserVisit::where( 'user_id', $_userId )
		                    ->whereDate( 'visit', '>', $carbon )
		                    ->get();
	}

	public function contractsThatAreBetween( int $_userId, Carbon $_start, Carbon $_end ) {
		return MobyUserMobyContract::where( 'user_id', $_userId )
		                           ->whereDate( 'valid_from', '>=', $_start )
		                           ->whereDate( 'valid_to', '>=', $_end )
		                           ->get();
	}

	public function contractsWithIdsWithLessThanXDaysRemaining( Array $_contractIds, int $_days , int $_userId ) {
		$date = ( new Carbon() )->addDays( $_days );

		//var_dump( $_userId );
		if ( $_userId  == 2259 ) {
			$a = MobyUserMobyContract::where( 'user_id', $_userId )
			                         ->whereHas( 'contract',
				                         function($q) use( $_contractIds ) {
					                         $q->whereIn( 'moby_item_id', $_contractIds );
				                         })
			                         ->whereDate( 'valid_to', '<=', $date )
			                         ->whereDate( 'valid_to', '>=', Carbon::now() )
			                         ->get();

			//var_dump( $a );
		}

		return MobyUserMobyContract::where( 'user_id', $_userId )
									->whereHas( 'contract',
										function($q) use( $_contractIds ) {
											$q->whereIn( 'moby_item_id', $_contractIds );
									})
		                           ->whereDate( 'valid_to', '<=', $date )
		                           ->whereDate( 'valid_to', '>=', Carbon::now() )
		                           ->get();
	}

	public function addEmailToList( string $_email ) {
		if ( ! $list_id = $this->getListId() ) return;
		$result  = $this->getMailChimp()->post( "lists/$list_id/members", [
			'email_address' => $_email,
			'status'        => 'subscribed',
			'merge_fields'  => [ 'FNAME' => "", 'LNAME' => "" ],
		] );

		/*var_dump( $list_id );
		var_dump( $result );*/
	}

	public function userHasCampaign( int $_userId ) {
		return UserCampaign::where( 'campaign_identifier', $this->campaignId )->where( 'user_id', $_userId )->get()->count() > 0;
	}

	public function userHasActiveCampaign( int $_userId ) {
		return UserCampaign::where( 'campaign_identifier', $this->campaignId )->where( 'user_id', $_userId )->where( 'is_closed', false )->get()->count() > 0;
	}

	public function userHasActiveCampaignStartedInPastYear( int $_userId ) {
		return UserCampaign::where( 'campaign_identifier', $this->campaignId )->whereDate( 'start_datetime', '>=', Carbon::now()->subYear() )->where( 'user_id', $_userId )->where( 'is_closed', false )->get()->count() > 0;
	}

	/**
	 * @param mixed $user
	 */
	public function parmUser( $_user = null ) {
		if ( is_null( $_user ) ) {
			return $this->user;
		}

		return $this->user = $_user;
	}

	public function logToDevVeyzon( string $_msg ) {
		file_get_contents( 'http://logger.dev.veyzon.com/?log=' . urlencode( $_msg ) );
	}

}






























