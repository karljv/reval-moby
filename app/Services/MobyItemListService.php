<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\MobyContract;
use App\Models\MobyUser;
use App\Models\MobyUserMobyContract;

class MobyItemListService {
    private $apiService;

    public function __construct() {
        $this->apiService = new ApiService();
    }

    public function processAndSaveItems() {
        $xml = $this->apiService->getItemList();

        if ( ! $xml ) throw new \Exception( "Endpoint is down." );

        foreach( $xml->RECORD as $record ) {
            $mobyItemId = (int) $record->ID;
            $mobyItemCode = (int) $record->CODE;
            $mobyItemName = (string) $record->NAME;

	        if ( ! $this->createOrUpdate( $mobyItemId, $mobyItemCode, $mobyItemName ) ) throw new \Exception( "Failed to save moby contract." );
        }
    }

    public function createOrUpdate(
    	int $_mobyItemId,
		int $_mobyItemCode,
		string $_mobyItemName
    ) {
	    if ( ! $mobyContract = MobyContract::where( 'moby_item_id', $_mobyItemId )->first() ) {
		    $mobyContract = new MobyContract();
	    }

	    $mobyContract->moby_item_id = $_mobyItemId;
	    $mobyContract->moby_item_code = $_mobyItemCode;
	    $mobyContract->moby_item_name = $_mobyItemName;

	    if ( ! $mobyContract->save() ) return false;

	    return $mobyContract;
    }
}