<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Models\SentEmail;
use App\Models\UserCampaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class AMPersonalTrainingOffer extends AMBaseService {




	//num on IMG: 2 - AM: Aktiivsed treenijad (personaaltreening)
	//Seems to be a stub




	const ID_CAMP                       = "CAMP_PTRAIN";

	const ID_MAIL_FIRST                 = 'PTRAIN_FIRST';
	const ID_MAIL_SECOND                = 'PTRAIN_SECOND';
	const ID_MAIL_THIRD                 = 'PTRAIN_THIRD';
	const ID_MAIL_FOURTH                = 'PTRAIN_FOURTH';

	const TIME_HOURS_UNTIL_SECOND_EMAIL = - 1;   //72
	const TIME_HOURS_UNTIL_THIRD_EMAIL  = - 1;   //168
	const TIME_HOURS_UNTIL_FOURTH_EMAIL  = - 1;   //168

	const IDS_MOBY_MONTHLY_PACKAGES = [ 577, 1055, 576, 1053, 578, 1504, 652, 585, 584 ];
	const IDS_MOBY_YEARLY_PACKAGES = [ 693, 760 ];
	const IDS_MOBY_MONTHLY_AND_YEARLY_PACKAGES = self::IDS_MOBY_MONTHLY_PACKAGES + self::IDS_MOBY_YEARLY_PACKAGES;

	public function __construct() {
		throw new \Exception( "Moby does not write down used PT COURSES. Consult Peeter Tars." );

		$this->init();
	}

	public function init() {
		parent::init();

		$this->setListId( "b954be2203" );
		$this->campaignId = self::ID_CAMP;
	}

	public function cron() {
		$this->addNewUsersToCampaigns( $this->calcUsersToAdd() );

		foreach ( $this->getActiveCampaigns() as $campaign ) {
			if ( $campaign->user ) {
				$this->setEmail( $campaign->user->email );
			} else {
				$this->setEmail( $campaign->externalUser->email );
			}

			$lastEmailSent = $campaign->sentEmailsOrderedDESC->first();

			switch ( $lastEmailSent->email_identifier ) {
				case self::ID_MAIL_FIRST:
					$this->sendSecondEmail();
					break;

				case self::ID_MAIL_SECOND:
					$this->sendThirdEmail();
					break;

				case self::ID_MAIL_THIRD:
					$this->sendFourthEmail();
					break;
			}
		}
	}


	/**
	 * SELECT user.email, contract.moby_item_name  FROM `reval-moby`.moby_users as user
	JOIN
	moby_user_moby_contract as c
	ON
	c.user_id = user.id
	JOIN
	moby_contracts as contract
	ON
	contract.id = c.contract_id
	WHERE
	contract.moby_item_id IN (577, 1055, 576, 1053, 578, 1504, 652, 585, 584 )
	;
	 *
	 */
	public function calcUsersToAdd() {
		$ret = [];
		$res = $this->getUsersWhoseContractMobyItemIdInAndContractIsValidAndValidFromIsLessThan( self::IDS_MOBY_MONTHLY_AND_YEARLY_PACKAGES, Carbon::now()->subWeek(2) );

		//->whereDate('DeadLine', '>', Carbon::now())

		foreach( $res as $user ) {
			//No active PT campaigns in the last year
			if ( $campaignInLastYear = UserCampaign::where( 'user_id', $user->id )
													  ->where( 'campaign_identifier', self::ID_CAMP )
					                                  ->whereBetween( 'start_datetime', [Carbon::now()->subYear(), Carbon::now()->addDay()] )
					                                  ->exists() ) {
				continue;
			}

			$ret[] = $user;
		}

		if ( env('DEVMODE') ) {
			if ( count( $ret ) ) {
				$user = MobyUser::where( 'email', $ret[0]->email )->first();
			} else {
				$user = MobyUser::where( 'email', 'anneli.aasamets@gmail.com' )->first();
			}
			$user->email = env( 'CUR_TEST_MAIL' );
			$user->save();
			$ret = [$user];
			/*if ( env( 'DEV_MODE_RANDOM_EMAILS' ) ) {
				$user = new MobyUser();
				$user->id = -1;
				$user->email = Uuid::generate(4) . "@hot.ee";
				$ret[] = $user;
			}*/
		}

		return $ret;
	}

	public function addNewUsersToCampaigns( $_users ) {
		foreach( $_users as $user ) {
			//echo $user->id . " > " . $user->email . "<br>";
			$this->setEmail( $user->email );
			$this->parmUser( $user );
			$this->sendFirstEmail();
		}
	}

	public function canSendFirstEmail() {
		if ( ! $this->parmUser() ) return false;
		if ( $this->userHasActiveCampaign( $this->parmUser()->id ) ) return false;

		return true;
	}

	public function sendFirstEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendFirstEmail() ) {
			return false;
		}


		$this->addEmailToList( $this->getEmail() );
		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/6a3b672a61/emails/f78867af8f/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					var_dump( $this->getEmail() );
					var_dump( $resObj );
					//ERROR
					break;

				default:
					var_dump( $this->getEmail() );
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			if ( ! $userCampaign = $this->getUserCampaign() ) {
				$userCampaign = new UserCampaign();

				$userOrExternalUser = $this->getUser();
				switch ( true )
				{
					case $userOrExternalUser instanceof MobyUser:
						$userCampaign->user_id = $userOrExternalUser->id;
						break;

					case $userOrExternalUser instanceof ExternalUser:
						$userCampaign->external_user_id = $userOrExternalUser->id;
						break;
				}

				$userCampaign->start_datetime = date('Y-m-d H:i:s' );
				$userCampaign->campaign_identifier = self::ID_CAMP;

				$userCampaign->saveOrFail();
			}

			$sentEmail->user_campaign_id = $userCampaign->id;
			$sentEmail->email_identifier = self::ID_MAIL_FIRST;

			$sentEmail->saveOrFail();

			$msg = ":AM> " . $this->getEmail() . " |- offer for a Personal Training -| SENT. [" . date("d.m.Y") . "]";
			Log::debug( ":AM> " . $this->getEmail() . " |- offer for a Personal Training -| SENT. [" . date("d.m.Y") . "]" );
			$this->logToDevVeyzon( $msg );
		}
	}

	public function canSendSecondEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === self::ID_MAIL_FIRST ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, self::TIME_HOURS_UNTIL_SECOND_EMAIL ) ) {
				//If user already has a yearly package, then exclude him and close campaing




				throw new \Exception( "Moby does not write down used PT COURSES. Consult Peeter Tars." );

				if ( ! count( $this->userHasValidContractInTimeFromIds( new Carbon(), $this->parmUser()->id, self::IDS_MOBY_YEARLY_PACKAGES ) ) > 0 ) {
					return true;
				} else {
					$this->endUserCampaign( $userCampaign );
				}

			}
		}

		return true;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendSecondEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendSecondEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/fe982423f9/emails/0bed15108f/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = self::ID_MAIL_SECOND;

			$sentEmail->saveOrFail();
		}
	}

	public function canSendThirdEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === self::ID_MAIL_SECOND ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, self::TIME_HOURS_UNTIL_THIRD_EMAIL ) ) {
				//If user already has a yearly package, then exclude him and close campaing
				if ( ! count( $this->userHasValidContractInTimeFromIds( new Carbon(), $this->parmUser()->id, self::IDS_MOBY_YEARLY_PACKAGES ) ) > 0 ) {
					return true;
				} else {
					$this->endUserCampaign( $userCampaign );
				}

			}
		}

		return false;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendThirdEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		if ( ! $this->canSendThirdEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/fe982423f9/emails/91c41b8fe0/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = self::ID_MAIL_THIRD;

			$sentEmail->saveOrFail();
		}
	}
}