<?php
/**
 * Created by PhpStorm.
 * User: karljv.
 * Date: 13/02/2018
 * Time: 14:42
 */

namespace App\Services;

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Models\SentEmail;
use App\Models\UserCampaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class AMNewsletterSubscription extends AMBaseService {

	//name in MC - AM: Tervitusemailide seeria (uudiskirjaga liitumisel)

	const ID_CAMP_NEWSLETTER_SUB = "CAMP_NEWSLETTER_SUB";

	const ID_MAIL_SUB                   = 'NEWSLETTER_SUB_SUB';
	const ID_MAIL_SECOND                = 'NEWSLETTER_SUB_SECOND';
	const ID_MAIL_THIRD                 = 'NEWSLETTER_SUB_THIRD';

	const TIME_HOURS_UNTIL_SECOND_EMAIL = 72;   //72
	const TIME_HOURS_UNTIL_THIRD_EMAIL  = 144;   //168

	public function __construct() {
		$this->init();
	}

	public function init() {
		parent::init();

		$this->setListId( "771e11056d" );
		$this->campaignId = AMNewsletterSubscription::ID_CAMP_NEWSLETTER_SUB;
	}

	public function cron() {
		foreach ( $this->getActiveCampaigns() as $campaign ) {
			if ( $campaign->user ) {
				$this->setEmail( $campaign->user->email );
			} else {
				$this->setEmail( $campaign->externalUser->email );
			}

			$lastEmailSent = $campaign->sentEmailsOrderedDESC->first();

			switch ( $lastEmailSent->email_identifier ) {
				case AMNewsletterSubscription::ID_MAIL_SUB:
					$this->sendSecondEmail();
					break;

				case AMNewsletterSubscription::ID_MAIL_SECOND:
					$this->sendThirdEmail();
					break;
			}
		}
	}

	public function userCameWithOneTimeTicket() {
		$AMOneTimeTicket = new AMOneTimeTicket();
		$AMOneTimeTicket->handleNewEmail( $this->getEmail() );
	}

	public function canSendSecondEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === AMNewsletterSubscription::ID_MAIL_SUB ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, AMNewsletterSubscription::TIME_HOURS_UNTIL_SECOND_EMAIL ) ) {
				//Cannot buy anything without an user, thus if there is no user, then nothing has been bought
				$userOrExternalUser = $this->getUser();
				if ( ! $userOrExternalUser->one_time_ticket_purchase_datetime ) {
					return true;
				} else {
					$this->userCameWithOneTimeTicket();
				}
			}
		}

		return false;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendSecondEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}
		if ( ! $this->canSendSecondEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/18b13feeb7/emails/a777ed4241/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = AMNewsletterSubscription::ID_MAIL_SECOND;

			$sentEmail->saveOrFail();
		}
	}

	public function canSendThirdEmail() {
		$userCampaign = $this->getUserCampaign();
		$lastMail     = $this->getLastSentEmailInCampaign( $userCampaign );

		if ( $lastMail->email_identifier === AMNewsletterSubscription::ID_MAIL_SECOND ) {
			//3 days after the first mail
			if ( $this->isItTimeToSendNextEmail( $lastMail, AMNewsletterSubscription::TIME_HOURS_UNTIL_THIRD_EMAIL ) ) {
				//Cannot buy anything without an user, thus if there is no user, then nothing has been bought

				$userOrExternalUser = $this->getUser();
				if ( ! $userOrExternalUser->one_time_ticket_purchase_datetime ) {
					return true;
				} else {
					$this->userCameWithOneTimeTicket();
				}
			}
		}

		return false;
	}

	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendThirdEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}
		if ( ! $this->canSendThirdEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/18b13feeb7/emails/594c47ddb0/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			$sentEmail->user_campaign_id = $this->getUserCampaign()->id;
			$sentEmail->email_identifier = AMNewsletterSubscription::ID_MAIL_THIRD;

			$sentEmail->saveOrFail();

			$this->endUserCampaign( $this->getUserCampaign() );
		}
	}

	public function newSubscription( $_email ) {
		$this->setEmail( $_email );

		if ( $mobyUser = MobyUser::where( 'email', $_email )->first() ) {
			if ( ! $mobyUser->newsletter_subscription_datetime ) {
				$mobyUser->newsletter_subscription_datetime = date("Y-m-d H:i:s" );
				if ( ! $mobyUser->save() ) throw new \Exception( "Failed to add user $_email" );

				$AMNewsletterSubscription = new AMNewsletterSubscription();
				$AMNewsletterSubscription->setEmail( $_email );

				$resObj = $this->addUserToList();
				$this->sendFirstEmail();
			} else {
				throw new \Exception( "$_email has already subscribed to the newsletter." );
			}
		}

		if ( ! $externalUser = ExternalUser::where( 'email', $_email )->first() ) {
			$externalUser = new ExternalUser();

			$externalUser->email = $_email;
		} else {
			if ( $externalUser->newsletter_subscription_datetime ) {
				echo $_email . " has already subscribed to the newsletter.";

				return;
			}
		}

		$externalUser->newsletter_subscription_datetime = date( "Y-m-d H:i:s" );

		if ( ! $externalUser->save() ) {
			throw new \Exception( "Contact Veyzon. Error." );
		}

		$AMNewsletterSubscription = new AMNewsletterSubscription();
		$AMNewsletterSubscription->setEmail( $_email );

		$resObj = $this->addUserToList();
		$this->sendFirstEmail();

		//var_dump( $resObj );

		if ( ! is_object( $resObj ) ) { //success

		}
	}


	/**
	 * Send the second email in the sequence.
	 * This email is sent if the user does not attend training withing 3 days after subscribing to the newsletter
	 *
	 * @return bool
	 */
	public function sendFirstEmail() {
		if ( ! $this->getEmail() ) {
			return false;
		}

		$resObj = $this->executeCurl( "https://us4.api.mailchimp.com/3.0/automations/18b13feeb7/emails/e3d58ec677/queue" );

		//['status']
		if ( is_object( $resObj ) ) {
			switch ( $resObj->status ) {
				case 400:
					echo "AMNewsletterSubscription::ID_MAIL_SUB> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					//ERROR
					break;

				default:
					echo "AMNewsletterSubscription::ID_MAIL_SUB> " . $this->getEmail() . "<br>";
					var_dump( $resObj );
					break;
			}
		} else {
			$sentEmail = new SentEmail();

			if ( ! $userCampaign = $this->getUserCampaign() ) {
				$userCampaign = new UserCampaign();

				$userOrExternalUser = $this->getUser();
				switch ( true )
				{
					case $userOrExternalUser instanceof MobyUser:
						$userCampaign->user_id = $userOrExternalUser->id;
						break;

					case $userOrExternalUser instanceof ExternalUser:
						$userCampaign->external_user_id = $userOrExternalUser->id;
						break;
				}

				$userCampaign->start_datetime = date('Y-m-d H:i:s' );
				$userCampaign->campaign_identifier = AMNewsletterSubscription::ID_CAMP_NEWSLETTER_SUB;

				$userCampaign->saveOrFail();
			}

			$sentEmail->user_campaign_id = $userCampaign->id;
			$sentEmail->email_identifier = AMNewsletterSubscription::ID_MAIL_SUB;

			$sentEmail->saveOrFail();
		}
	}

}