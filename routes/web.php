<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\ExternalUser;
use App\Models\MobyUser;
use App\Services\AMNewsletterSubscription;
use App\Services\AMOneTimeTicket;
use Carbon\Carbon;
use Illuminate\Http\Request;

Route::get('/playground', function () {
    return view('playground');
});

Route::get('/admin-forms', function () {
    return view('admin-forms');
});

Route::get('/join-newsletter', function ( Request $request ) {
	if ( ! $request->has( 'email' ) ) throw new \Exception( "Invalid input" );

	$email = $request->input( 'email' );

	$AMNewsletterSubscription = new AMNewsletterSubscription();
	$AMNewsletterSubscription->newSubscription( $email );

	echo "subscribed $email";
	return;
});




Route::get('/submit-newsletter-came-to-training', function ( Request $request ) {
    if ( ! $request->has( 'email' ) ) throw new \Exception( "Invalid input" );

	$email = $request->input( 'email' );

	$AMNewsletterSubscription = new AMNewsletterSubscription();
	$AMNewsletterSubscription->setEmail( $email );
	$AMNewsletterSubscription->endUserCampaign( $AMNewsletterSubscription->getUserCampaign() );

	$AMOneTimeTicket = new AMOneTimeTicket();
	$AMOneTimeTicket->handleNewEmail( $email );
});